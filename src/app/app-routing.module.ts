import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ListComponent } from './admin/articles/list/list.component';
import { CreateComponent } from './admin/articles/create/create.component';
import { UpdateComponent } from './admin/articles/update/update.component';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'listado', component: ListComponent },
  { path: 'crear', component: CreateComponent },
  { path: 'editar/:id', component: UpdateComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
