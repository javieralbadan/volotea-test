import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
  items = [
    { text: 'Home', route: '/' },
    { text: 'Listado de noticias', route: '/listado' },
    { text: 'Crear noticia', route: '/crear' },
  ];

  constructor() {}

  ngOnInit(): void {}
}
