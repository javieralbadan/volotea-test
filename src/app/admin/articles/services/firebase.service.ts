import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Article, ArticleInterface } from '@app/entities/article';
import { map, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class FirestoreService {
  private dbCollection = 'articles';

  constructor(private firestore: AngularFirestore) {}

  public createArticle(data: {
    active: boolean;
    creationDate: string;
    category: string;
    title: string;
    subtitle: string;
    image: string;
    content: string;
  }) {
    data.image ? (data.image = '') : null;
    return this.firestore.collection(this.dbCollection).add(data);
  }

  public getArticle(documentId: string) {
    return this.firestore
      .collection(this.dbCollection)
      .doc(documentId)
      .snapshotChanges()
      .pipe(
        filter((actionSnapshot) => {
          return actionSnapshot.payload.data() instanceof Object;
        }),
        map((actionSnapshot) => {
          const data = actionSnapshot.payload.data();
          if (data instanceof Object) {
            return new Article({ id: documentId, ...data });
          }
          return null;
        }),
      );
  }

  public getArticles() {
    return this.firestore
      .collection(this.dbCollection)
      .snapshotChanges()
      .pipe(
        map((actionSnapshot) => {
          return actionSnapshot
            .map((data: any) => new Article({ id: data.payload.doc.id, ...data.payload.doc.data() }))
            .sort((a, b) => {
              if (a.creationDate > b.creationDate) {
                return -1;
              }
              if (a.creationDate < b.creationDate) {
                return 1;
              }
              return 0;
            });
        }),
      );
  }

  public updateArticle(documentId: string, article: ArticleInterface) {
    article.image ? (article.image = '') : null;
    return this.firestore
      .collection(this.dbCollection)
      .doc(documentId)
      .set(article);
  }

  public deleteArticle(documentId: string) {
    return this.firestore
      .collection(this.dbCollection)
      .doc(documentId)
      .delete();
  }
}
