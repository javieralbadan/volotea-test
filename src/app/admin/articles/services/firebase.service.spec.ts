import { fakeAsync, TestBed } from '@angular/core/testing';

import { FirestoreService } from './firebase.service';

describe('FirestoreService', () => {
  let service: FirestoreService;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FirestoreService);
  }));

  it('should be created', fakeAsync(() => {
    expect(service).toBeTruthy();
  }));
});
