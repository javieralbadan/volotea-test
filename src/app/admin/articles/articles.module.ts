import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from '@app/app-routing.module';
import { AngularFirestore } from '@angular/fire/firestore';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
import { UpdateComponent } from './update/update.component';
import { ListCardComponent } from './components/list-card/list-card.component';
import { PopupComponent } from './components/popup/popup.component';
import { FormComponent } from './components/form/form.component';

@NgModule({
  declarations: [ListComponent, CreateComponent, UpdateComponent, ListCardComponent, PopupComponent, FormComponent],
  exports: [],
  imports: [CommonModule, AppRoutingModule, FormsModule, ReactiveFormsModule],
  providers: [AngularFirestore],
})
export class ArticlesModule {}
