import { Component, OnInit, ViewChild } from '@angular/core';
import { FirestoreService } from '../services/firebase.service';
import { FormComponent } from '../components/form/form.component';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import * as moment from 'moment';
declare var $: any;

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit {
  @ViewChild('articleForm') articleForm: FormComponent;

  loading = false;
  newArticle = undefined;
  popupInfo = {
    title: '',
    subtitle: '',
    titleArticle: '',
  };

  constructor(private firestoreService: FirestoreService, private toastr: ToastrService, private router: Router) {}

  ngOnInit(): void {}

  openPopup(item: any) {
    this.newArticle = item;
    this.popupInfo.title = 'Crear noticia';
    this.popupInfo.subtitle = '¿Estás seguro de crear esta noticia?';
    this.popupInfo.titleArticle = item.title;
    $('#delete-popup').modal('show');
  }

  confirmCreate() {
    this.loading = true;
    moment.locale('es');
    this.newArticle.creationDate = moment().format('YYYY-MM-DD HH:mm');
    try {
      this.firestoreService.createArticle(this.newArticle).then(() => {
        this.toastr.success('Artículo creado!');
        this.loading = false;
        this.router.navigateByUrl('/listado');
      });
    } catch (error) {
      this.toastr.error(`Error al crear: ${error}`);
      this.loading = false;
    }
  }
}
