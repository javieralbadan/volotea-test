import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss'],
})
export class PopupComponent implements OnInit {
  @Input() popupInfo: any;
  @Output() confirm = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  actionConfirm() {
    this.confirm.emit();
  }

  resetInfo() {
    this.popupInfo.title = '';
    this.popupInfo.subtitle = '';
    this.popupInfo.titleArticle = '';
  }
}
