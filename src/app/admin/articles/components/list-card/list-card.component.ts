import { Component, Input, Output, EventEmitter } from '@angular/core';
import { categories } from '@app/constants/article-categories';
import * as moment from 'moment';
@Component({
  selector: 'app-list-card',
  templateUrl: './list-card.component.html',
  styleUrls: ['./list-card.component.scss'],
})
export class ListCardComponent {
  @Input() item: any;
  @Output() deleting = new EventEmitter();

  defaultImage =
    'https://firebasestorage.googleapis.com/v0/b/volotea-app.appspot.com/o/default-news.jpg?alt=media&token=84003255-f91c-4fa2-bf5d-ffba8590e916';
  categoryLabel = '';
  backgroundColor = '';
  categories = categories;

  constructor() {}

  ngOnInit() {
    this.getColorCard();
  }

  getColorCard() {
    let category = this.item ? this.item.category : null;
    if (category) {
      this.categoryLabel = category in this.categories ? this.categories[category].text : 'Sin categoría';
      this.backgroundColor = category in this.categories ? this.categories[category].color : 'grey';
    }
  }

  moment(date) {
    moment.locale('es');
    return moment(date).fromNow();
  }

  deleteArticle() {
    this.deleting.emit(this.item);
  }
}
