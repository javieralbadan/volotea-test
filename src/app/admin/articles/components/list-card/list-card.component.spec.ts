import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCardComponent } from './list-card.component';

describe('ListCardComponent', () => {
  let component: ListCardComponent;
  let fixture: ComponentFixture<ListCardComponent>;
  const itemExample = {
    active: '',
    creationDate: '',
    category: '',
    title: '',
    subtitle: '',
    image: '',
    content: '',
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListCardComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCardComponent);
    component = fixture.componentInstance;
    component.item = itemExample;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
