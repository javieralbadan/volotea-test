import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, EventEmitter } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { FormComponent } from './form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FirestoreService } from '../../services/firebase.service';
import { ArticlesModule } from '../../articles.module';
import { ToastrModule } from 'ngx-toastr';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'environments/environment';
declare var $: any;

beforeEach(async(() => {
  TestBed.configureTestingModule({
    declarations: [FormComponent],
    imports: [
      ReactiveFormsModule,
      AngularFireModule.initializeApp(environment.firebase),
      ToastrModule.forRoot(),
      ArticlesModule,
    ],
    providers: [FirestoreService, ToastrService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
  }).compileComponents();
}));

describe('FormComponent', () => {
  let component: FormComponent;
  let fixture: ComponentFixture<FormComponent>;
  const eventKeyUp: Event = new KeyboardEvent('keyup', {
    code: 'Escape',
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('test updateTitleRule method', () => {
    fixture.whenStable().then(() => {
      window.dispatchEvent(eventKeyUp);
      expect(component.updateTitleRule).toHaveBeenCalled();
    });
  });

  it('test updateSubtitleRule method', () => {
    fixture.whenStable().then(() => {
      window.dispatchEvent(eventKeyUp);
      expect(component.updateSubtitleRule).toHaveBeenCalled();
    });
  });
});

describe('FormComponent save', () => {
  let component: FormComponent;
  let fixture: ComponentFixture<FormComponent>;

  beforeEach(() => {
    fixture = TestBed.createComponent(FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('test saveArticle method without valid form', () => {
    fixture.whenStable().then(() => {
      const toastr = {
        error: (message: string) => {},
      };
      const spyOnToastr = spyOn(toastr, 'error');
      expect(component.saveArticle).toHaveBeenCalled();
      expect(spyOnToastr).toHaveBeenCalledWith('Existen campos inválidos');
    });
  });

  it('test saveArticle method with valid form', () => {
    component.articleForm.value.title = 'true';
    component.articleForm.value.subtitle = 'true';
    component.articleForm.value.category = 'true';
    component.formValidate = true;
    const spyOnEmit = spyOn(component.finish, 'emit').and.callThrough();
    fixture.whenStable().then(() => {
      let event: Event = new CustomEvent('submit');
      window.dispatchEvent(event);
      expect(component.saveArticle).toHaveBeenCalled();
      expect(spyOnEmit).toHaveBeenCalledWith(component.articleForm.value);
    });
  });
});
