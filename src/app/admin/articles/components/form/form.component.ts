import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FirestoreService } from '../../services/firebase.service';
import { categories } from '@app/constants/article-categories';
declare var $: any;

@Component({
  selector: 'app-form-article',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  @Input() loading: boolean;
  @Input() textButton: string;
  @Output() finish = new EventEmitter();

  articleFormId: string;
  articleForm: FormGroup;
  formValidate = false;
  categories = Object.values(categories);

  constructor(
    private formBuilder: FormBuilder,
    private firestoreService: FirestoreService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
  ) {
    this.articleForm = this.formBuilder.group({
      active: [''],
      category: ['', Validators.required],
      title: ['', Validators.required],
      subtitle: ['', Validators.required],
      content: [''],
      image: [''],
      creationDate: [''],
    });
    this.route.paramMap.subscribe((params) => {
      if (params.get('id')) {
        this.loadDataForm(params.get('id'));
      }
    });
  }

  ngOnInit(): void {
    $('.ui.checkbox').checkbox();
    this.loadingRulesValidation();
  }

  loadDataForm(paramId) {
    this.firestoreService.getArticle(paramId).subscribe((article) => {
      this.articleFormId = paramId;
      this.articleForm.setValue({
        active: article.active,
        category: article.category,
        title: article.title,
        subtitle: article.subtitle,
        content: article.content,
        creationDate: article.creationDate,
        image: '',
      });
    });
  }

  loadingRulesValidation() {
    $('.ui.form').form({
      on: 'blur',
      inline: true,
      fields: {
        category: {
          identifier: 'category',
          optional: false,
          rules: [{ type: 'empty', prompt: 'Campo requerido' }],
        },
      },
      onInvalid: () => {
        this.formValidate = false;
      },
      onValid: () => {
        this.formValidate = true;
      },
    });
  }

  updateTitleRule() {
    let minLength = this.articleForm.value.title.split('').length;
    $('.ui.form').form('add rule', 'subtitle', {
      rules: [
        { type: 'empty', prompt: 'Campo requerido' },
        {
          type: `minLength[${minLength}]`,
          prompt: 'Mínimo {ruleValue} caracteres (No puede ser menor que el título)',
        },
      ],
    });
  }

  updateSubtitleRule() {
    let maxLength = this.articleForm.value.subtitle.split('').length;
    if (maxLength > 0) {
      $('.ui.form').form('add rule', 'title', {
        rules: [
          { type: 'empty', prompt: 'Campo requerido' },
          { type: 'regExp[^[a-zñáéíóúA-ZÑÁÉÍÓÚ ]*$]', prompt: 'Este campo admite solo letras' },
          {
            type: `maxLength[${maxLength}]`,
            prompt: `Máximo {ruleValue} caracteres (No puede ser mayor que el subtítulo)`,
          },
        ],
      });
    }
  }

  saveArticle() {
    $('.ui.form').form('validate form');
    if (this.articleForm.valid && this.formValidate) {
      this.finish.emit(this.articleForm.value);
    } else {
      this.toastr.error('Existen campos inválidos');
    }
  }

  clearForm() {
    this.articleForm.reset({
      id: '',
      active: '',
      category: '',
      title: '',
      subtitle: '',
      content: '',
      creationDate: '',
      image: '',
    });
  }
}
