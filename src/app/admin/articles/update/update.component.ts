import { Component, OnInit, ViewChild } from '@angular/core';
import { FirestoreService } from '../services/firebase.service';
import { ArticleInterface } from '@app/entities/article';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
declare var $: any;

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss'],
})
export class UpdateComponent implements OnInit {
  @ViewChild('articleForm') articleForm: ArticleInterface;

  loading = false;
  editArticleId = '';
  editArticle = undefined;
  popupInfo = {
    title: '',
    subtitle: '',
    titleArticle: '',
  };

  constructor(
    private firestoreService: FirestoreService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
  ) {
    this.route.paramMap.subscribe((params) => {
      if (params.get('id')) {
        this.editArticleId = params.get('id');
      }
    });
  }

  ngOnInit(): void {}

  openPopup(item: any) {
    this.editArticle = item;
    this.popupInfo.title = 'Editar noticia';
    this.popupInfo.subtitle = '¿Estás seguro de actualizar esta noticia?';
    this.popupInfo.titleArticle = item.title;
    $('#delete-popup').modal('show');
  }

  confirmUpdate() {
    this.loading = true;
    try {
      debugger;
      this.firestoreService.updateArticle(this.editArticleId, this.editArticle).then(() => {
        this.toastr.success('Artículo actualizado!');
        this.loading = false;
      });
    } catch (error) {
      this.toastr.error(`Error al actualizar: ${error}`);
      this.loading = false;
    }
  }
}
