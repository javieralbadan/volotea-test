import { Component, OnInit } from '@angular/core';
import { FirestoreService } from '../services/firebase.service';
import { ToastrService } from 'ngx-toastr';
declare var $: any;

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  articles = undefined;
  popupInfo = {
    title: '',
    subtitle: '',
    titleArticle: '',
    idDeleting: '',
  };

  constructor(private firestoreService: FirestoreService, private toastr: ToastrService) {
    this.articles = this.firestoreService.getArticles();
  }

  ngOnInit() {}

  openPopup(item: any) {
    this.popupInfo.title = 'Eliminar noticia';
    this.popupInfo.subtitle = '¿Deseas eliminar permanentemente esta noticia?';
    this.popupInfo.titleArticle = item.title;
    this.popupInfo.idDeleting = item.id;
    setTimeout(() => {
      $('#delete-popup').modal('show');
    }, 100);
  }

  confirmDelete() {
    try {
      this.firestoreService.deleteArticle(this.popupInfo.idDeleting).then(() => {
        this.toastr.success('Artículo eliminado!');
      });
    } catch (error) {
      this.toastr.error(`Error al eliminar: ${error}`);
    }
  }
}
