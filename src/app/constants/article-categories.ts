export const categories = {
  general: {
    id: 'general',
    text: 'General',
    color: 'grey',
  },
  deportes: {
    id: 'deportes',
    text: 'Deportes',
    color: 'green',
  },
  politica: {
    id: 'politica',
    text: 'Política',
    color: 'yellow',
  },
  economia: {
    id: 'economia',
    text: 'Economía',
    color: 'red',
  },
  cultura: {
    id: 'cultura',
    text: 'Cultura',
    color: 'blue',
  },
};
