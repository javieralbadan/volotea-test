export type ArticleCategory = '' | 'general' | 'deportes' | 'politica' | 'economia' | 'cultura';

export interface ArticleInterface {
  id: string;
  active?: boolean;
  creationDate?: string;
  category?: ArticleCategory;
  title?: string;
  subtitle?: string;
  image?: string;
  content?: string;
}

export class Article {
  id: string = '';
  active: boolean = true;
  creationDate: string = '';
  category: ArticleCategory = '';
  title: string = '';
  subtitle: string;
  image: string;
  content: string;
  constructor(documentData: ArticleInterface) {
    Object.assign(this, documentData);
  }
}
